package com.slalom.scala.part1

import org.scalatest.AsyncFlatSpec

import scala.concurrent.Future

class Futures extends AsyncFlatSpec {

  behavior of "A Future"

  it should "eventually return an int" in {
    def addEventually(addends: Int*): Future[Int] = Future { addends.sum }

    val futureSum: Future[Int] = addEventually(1, 2, 3)
    futureSum map{sum => assert(sum == 6)}
  }
}
